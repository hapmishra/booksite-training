import { Component, OnInit } from '@angular/core';
import {NetService} from '../net.service';
import {Router, ActivatedRoute, ParamMap} from '@angular/router';
import {AuthService} from '../auth.service';

@Component({
  selector: 'app-book',
  templateUrl: './book.component.html',
  styleUrls: ['./book.component.css']
})
export class BookComponent implements OnInit {

  constructor(private netservice: NetService, private route: ActivatedRoute,private authservice:AuthService) { }


  data;
  paval;
  page;

  pagenum:number=1;
  url = 'http://advjavaapp-env.imadbprai6.us-west-2.elasticbeanstalk.com/booksapp/books?page='+this.pagenum;
  getdata(): void {
    var url_copy=this.url;
    this.route.queryParamMap.subscribe(params=>{this.paval=params.get('newarrival')})
    console.log(this.paval)
    if(this.authservice.url_modify=='yes'){
      url_copy='http://advjavaapp-env.imadbprai6.us-west-2.elasticbeanstalk.com/booksapp/books'+'?newarrival='+this.paval+'&page='+this.pagenum;
      console.log(url_copy)
    }

    console.log(this.authservice.url_modify)
    if(this.authservice.url_modify=='/fiction'){
      console.log(1)
      url_copy='http://advjavaapp-env.imadbprai6.us-west-2.elasticbeanstalk.com/booksapp/books'+'/fiction?page='+this.pagenum;
    }
    if(this.authservice.url_modify=='/children'){
      url_copy='http://advjavaapp-env.imadbprai6.us-west-2.elasticbeanstalk.com/booksapp/books'+'/children?page='+this.pagenum;
    }
    if(this.authservice.url_modify=='/management'){
      url_copy='http://advjavaapp-env.imadbprai6.us-west-2.elasticbeanstalk.com/booksapp/books'+'/management?page='+this.pagenum;
    }
    if(this.authservice.url_modify=='/mystery'){
      url_copy='http://advjavaapp-env.imadbprai6.us-west-2.elasticbeanstalk.com/booksapp/books'+'/mystery?page='+this.pagenum;
    }
    if(this.authservice.url_modify=='allbooks'){
      url_copy='http://advjavaapp-env.imadbprai6.us-west-2.elasticbeanstalk.com/booksapp/books?page='+this.pagenum;
    }
    console.log(url_copy)
    this.netservice.getData(url_copy).subscribe(rasp => {this.data = rasp;});
    this.page=this.data.pageInfo
    console.log(this.page)
  }
  //page counter functions and varibales
  initialCount=1;
  totalcount:number=0;
  count(n:number){
    this.totalcount=n;
    console.log(this.pagenum)
  }
  //next button
  next(){
    this.pagenum=this.pagenum+1;
    this.authservice.nextpage(this.pagenum)
  }
  previous(){
    this.pagenum=this.pagenum-1;
    this.authservice.nextpage(this.pagenum)
  }

  ngOnInit() {
    this.authservice.urlobs1.subscribe(
      status=>{
        this.getdata();
      }
    )
    this.authservice.pageobs1.subscribe(status=>{this.getdata()})
    this.getdata();
  }

}
