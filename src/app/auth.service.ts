import { Injectable } from '@angular/core';
import {Observable,Subject} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  constructor() { }
  private urlobs=new Subject<{String}>();
  public urlobs1=this.urlobs.asObservable();

  public pageobs=new Subject<{Number}>();
  public pageobs1=this.pageobs.asObservable();

  url_modify;
  pagenumber;

  fetchurl(s:String){
    this.url_modify=s;
    console.log(s)
    this.urlobs.next(this.url_modify)
    this.url_modify;
  }
  nextpage(n:number){
    this.pagenumber=n;
    this.pageobs.next(this.pagenumber)
  }
}
