import { Component } from '@angular/core';
import {AuthService} from './auth.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'booksite';

  constructor(private authservice:AuthService){}

  event(s:string){
    this.authservice.fetchurl(s);
  }
  newarrival(){
    this.authservice.fetchurl('yes')
  }
}
